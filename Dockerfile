FROM ubuntu

WORKDIR /app

ENV PATH=/root/.local/bin:$PATH

RUN apt-get update -y
RUN apt-get install -y python3-pip python-dev
#RUN pip3 install awscli

COPY requirements.txt ./

#RUN python -m stanza.downloader en

RUN pip3 install -r requirements.txt



COPY . ./

RUN chmod a+x *.py

#COPY . /app
#ENTRYPOINT ["python"]
#CMD ["python", "./extract_NE.py"]


ENTRYPOINT ["./extract_NE.py"]
