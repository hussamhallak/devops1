#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from boilerpy3 import extractors
from googletrans import Translator
from collections import Counter
import stanza
import sys

stanza.download('en')

nlp = stanza.Pipeline(lang='en' , processors='tokenize,ner')

translator = Translator()
extractor = extractors.ArticleExtractor()

TRACKED_CLASSES = ["GPE", "PERSON", "ORG"]

def extract(url):

	try:
		content = extractor.get_content_from_url(url)
	except:
		print("This URL did not return a status code of 200. Try a different URL.")
		return
		
	if len(content) > 5000:
		print('The text in this page exceeds 5000 character limit!')
		return

	output = translator.translate(content)

	translated_content = output.text

	doc = nlp(translated_content)
	
	nes = []
	
	for item in doc.entities:
		#if item.type in TRACKED_CLASSES:
		nes.append((item.text, item.type))
			
	dedup_tags = Counter(nes)

	res = {k: [] for k in TRACKED_CLASSES}
	for item, count in dedup_tags.items():
		if item[1] in TRACKED_CLASSES:
			#ar_item = translator.translate(item[0], src='en', dest='ar')
			res[item[1]].append((item[0], count))
	return res


if __name__ == "__main__":
	if len(sys.argv) != 2:
		print ("Usage: Python gts.py <url>")
		print ("e.g: python gts.py http://example.com")
		sys.exit()
	else:
		ext_entities = extract(sys.argv[1])
		if not ext_entities:
			sys.exit()
		for cat in TRACKED_CLASSES:
			print("----------- " + cat + " -------------")
			for e in ext_entities[cat]:
				print(e[0], e[1])
